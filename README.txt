
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Tips is an agnostic tooltip framework which allows management of tooltip
settings and content via a UI. Tips allows placement of tooltips on any page
element. The framework aims to allow for easy implementation of 3rd party
tooltip libraries. Integrating an external library is as simple as defining
it in the Tips info hook and listing out each setting variable as a form
element.

Tips provides integration for:

    * Smallipop
    * Tooltipster
    * Opentip

The intended audience for this module is site builders. Even defining new
tooltip libraries should be relatively straight forward using the current
examples provided. Please help by creating patches with additional library
integrations.


REQUIREMENTS
------------

Libraries API:  https://www.drupal.org/project/libraries
Token:          https://www.drupal.org/project/token
jQuery Update:  https://www.drupal.org/project/jquery_update

One of the following tooltip js libraries:

OpenTip:        http://www.opentip.org/
Smallipop:      http://projects.sebastianhelzle.net/jquery.smallipop/
Tooltipster:    http://iamceege.github.io/tooltipster/


INSTALLATION
------------

Download one of the js libraries mentioned in the REQUIREMENTS section above
and place in your /libraries directory (usually sites/all/libraries).

    * Opentips main js should be accessible via:
        libraries/downloads/opentip-jquery.min.js

    * Smallipop main js should be accessible via:
        libraries/lib/jquery.smallipop.min.js

    * Tooltipster main js should be accessible via:
        libraries/js/tooltipster.bundle.min.js

Download the module and place in your modules directory.

Install the module dependencies and enable via the UI at /admin/module
or use drush:
    drush en tips -y

Now enable the submodule that corresponds to the library you wish to use.
For example:
    drush en tips_opentip
    drush en tips_smallipop
    drush en tips_tooltipster


CONFIGURATION
-------------

    * Tips defines a custom text format which can be administered here:
    /admin/config/content/formats/tips
    You may need to add permissions to use this text format or you will be
    unable to add content to a content group (see below).

    * Add a setting group at:
    http://dev.platform.millgroup.local/admin/config/user-interface/tips
    (Setting groups tab).
    If you library is installed and the module is enabled, you will see a
    dropdown box and will be able to add the library.

    * Add a content group (a tooltip) at:
    /admin/config/user-interface/tips (Content groups tab)

    * Now you can combine the setting with the content and set the selector
    that will invoke the popup:
    /admin/config/user-interface/tips (Selectors tab).
    For example:
        Selector:       .page-title
        Pages:          admin/*
        Settings ID:    1
        Content ID:     1


MAINTAINERS
-----------

https://www.drupal.org/u/swim
https://www.drupal.org/u/rjjakes